DO $$ 
BEGIN
    IF NOT EXISTS (SELECT FROM pg_user WHERE usename = 'rentaluser') THEN
        CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
        GRANT CONNECT ON DATABASE dvdrental TO rentaluser;
    END IF;
END $$;

GRANT SELECT ON customer TO rentaluser;

SET ROLE rentaluser;
SELECT * FROM customer;
RESET ROLE;

DO $$ 
BEGIN
    IF NOT EXISTS (SELECT FROM pg_group WHERE groname = 'rental') THEN
        CREATE GROUP rental;
    END IF;
END $$;

ALTER GROUP rental ADD USER rentaluser;

GRANT UPDATE ON rental TO rental;
GRANT INSERT ON rental TO rental;

SET ROLE rentaluser;
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, staff_id) 
VALUES (32295, current_timestamp, 1, 1, 1);

UPDATE rental SET staff_id = 1 WHERE rental_id = 32290; 
RESET ROLE;

REVOKE INSERT ON rental FROM rental;

SET ROLE rentaluser;
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, staff_id) 
VALUES (32296, current_timestamp, 1, 1, 1);
RESET ROLE;

DO $$ 
BEGIN
    IF NOT EXISTS (SELECT FROM pg_roles WHERE rolname = 'client_MARY_SMITH') THEN
        CREATE ROLE client_MARIA_MILLER;
        GRANT USAGE ON SCHEMA public TO client_MARIA_MILLER;
        GRANT SELECT ON rental TO client_MARIA_MILLER;
        GRANT SELECT ON payment TO client_MARIA_MILLER;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO client_MARIA_MILLER;
    END IF;
END $$;

SET ROLE client_MARIA_MILLER;
SELECT * FROM rental WHERE customer_id = 7;
SELECT * FROM payment WHERE customer_id = 7;
RESET ROLE;
